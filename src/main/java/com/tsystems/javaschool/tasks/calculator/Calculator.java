package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Stack;


public class Calculator {

    private Stack<String> stack = new Stack<>();
    private Stack<Double> stack2 = new Stack<>();
    private String postfix = "";

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        if (statement == null || statement == "" || statement.matches(".*[.+\\*/\\-]{2,}.*")
                || !statement.matches("[0-9+\\-\\*/.()]*") || !checkBrackets(statement)) return null;


        for (int i = 0; i < statement.length(); i++){
            char current = statement.charAt(i);

            switch (current){
                case '+':
                    postfix = postfix + " ";
                    parseOperator(current, 1);

                    break;
                case '-':
                    postfix = postfix + " ";
                    parseOperator(current, 1);
                    break;
                case '*':
                    postfix = postfix + " ";

                    parseOperator(current, 2);

                    break;
                case '/':
                    postfix = postfix + " ";

                    parseOperator(current, 2);
                    break;
                case '(':
                    stack.push(String.valueOf(current));
                    break;
                case ')':
                    parseSubexpression();
                    break;
                    default:
                        postfix = postfix + current;
                        break;
            }
        }

        while (!stack.isEmpty()){
            postfix = postfix + " ";
            postfix = postfix + stack.pop();
        }
        postfix.trim();

        double result = calculate(postfix);
        if (result == Double.POSITIVE_INFINITY || result == Double.NEGATIVE_INFINITY || result == Double.NaN) return null;

        DecimalFormat twoDForm = new DecimalFormat("#.####");
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        twoDForm.setDecimalFormatSymbols(dfs);
        double finalResult =  Double.valueOf(twoDForm.format(result));

        if (finalResult % 1.0 != 0)
            return String.format("%s", finalResult);
        else
            return String.format("%.0f",finalResult);
    }

    public void parseOperator(char newChar, int priority){
        while (stack.isEmpty()==false){
            String top = stack.pop();
            if (top.equals("(")) {
                stack.push(top);
                break;
            } else {
              int priority2;
              if (top.equals("+") || top.equals("-")) priority2 = 1;
              else priority2 = 2;

              if (priority2 < priority) {
                  stack.push(top);
                  break;
              } else {

                postfix = postfix + top;
              }
            }
        }
        stack.push(String.valueOf(newChar));
    }

    public boolean checkBrackets(String str){
        int counter = 0;
        for (Character character: str.toCharArray()){
            if (character == '(') counter++;
            else if (character == ')') counter--;
        }
        if (counter == 0) return true;
        else return false;
    }

    public void parseSubexpression(){
        while (!stack.isEmpty()){
            String top = stack.pop();
            if (top.equals("(")) {
                break;
            } else {
                postfix = postfix + top;
            }
        }
    }

    public double calculate(String str){
        char ch;
        double num1, num2, result;
        boolean prevIsDigit = false;
        String temp = "";

        for (int j = 0; j < str.length(); j++){
            ch = str.charAt(j);
            if (ch == ' '){
                prevIsDigit = false;
                temp = "";
            } else if (ch >= '0' && ch <= '9' || ch == '.') {
                if (prevIsDigit){
                    temp += ch;
                    stack2.pop();
                    stack2.push(Double.valueOf(temp));
                } else {
                    stack2.push((double) ch - '0');
                    temp = String.valueOf(ch);
                    prevIsDigit = true;
                }
            } else {
                prevIsDigit = false;
                num2 = stack2.pop();
                num1 = stack2.pop();

                switch (ch){
                    case '+':
                        result = num1 + num2;
                        break;
                    case '-':
                        result = num1 - num2;
                        break;
                    case '*':
                        result = num1 * num2;
                        break;
                    case '/':
                        result = num1 / num2;
                        break;
                    default:
                        result = 0;
                        break;
                }
                stack2.push(result);
            }
        }
        result = stack2.pop();
        return result;
    }

}
