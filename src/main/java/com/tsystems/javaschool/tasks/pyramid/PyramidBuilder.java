package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    private List<Integer> sizes = new ArrayList<>();
    private List<Integer> cells = new ArrayList<>();

    public static void main(String[] args) {
        PyramidBuilder pyramidBuilder = new PyramidBuilder();
        pyramidBuilder.buildPyramid(Arrays.asList(1, 3, 2, 9, 4, 5));
    }

        /**
         * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
         * from left to right). All vacant positions in the array are zeros.
         *
         * @param inputNumbers to be used in the pyramid
         * @return 2d array with pyramid inside
         * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
         */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        fillSizes();
        fillCells();

        if (!(sizes.contains(inputNumbers.size())) || inputNumbers.contains(null)) throw new CannotBuildPyramidException();

        int size = inputNumbers.size();

        int index = sizes.indexOf(size);

        int numberOfRows = index + 2;

        int numberOfCells = cells.get(index);

        Collections.sort(inputNumbers);

        int[][] pyramid = new int[numberOfRows][numberOfCells];

        int tempIndex = 0;

        for (int i = 0; i < numberOfRows; i++){
            for (int j = 0; j < numberOfCells; j++){
                pyramid[i][j] = 0;
            }
        }

        for (int i = 0; i < numberOfRows; i++){
            int count = 0;
            int cell = index+1;
            for (int j = 0; j < numberOfCells; j++){
                pyramid[i][cell] = inputNumbers.get(tempIndex);
                cell+=2;
                count++;
                tempIndex++;
                if (i-count<0) {
                    index--;
                    break;
                }
            }
        }

        return pyramid;
    }

    public void fillSizes(){
        int temp = 0;

        for (int i = 1; i < 100; i++){
            temp = i + temp;
            if (i == 1) continue;
            sizes.add(temp);
        }
    }

    public void fillCells(){
        for (int i = 3; i < 100; i+=2){
            cells.add(i);
        }
    }

}
